package main

import (
	"github.com/Sirupsen/logrus"
	"net/http"
	"strconv"
	"fmt"
) 

var (
	log = logrus.WithField("cmd", "sqs-websockets")
	authorized_path = []string{"/"}
)

func main() {
	config, err := setConfig()
	if err != nil {
		log.WithField("configErr", err).Fatal("Config file not found")
	}
	authorized_path = append(authorized_path, config.GetStringSlice("authorized_path")...)
	http.HandleFunc("/", handleWebSocket)
	fmt.Println(config.GetString("host"), ":", strconv.Itoa(config.GetInt("port")))
	log.Println(http.ListenAndServe(config.GetString("host") + ":" + strconv.Itoa(config.GetInt("port")), nil))
}