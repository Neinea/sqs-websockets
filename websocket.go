package main

import (
	"net/http"
	"fmt"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader {
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func handleWebSocket(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	defer conn.Close()

	if err != nil {
		errorMessage := "Unable to upgrade to websockets"
		log.WithField("err", err).Println(errorMessage)
		http.Error(w, errorMessage, http.StatusBadRequest)
		return
	}

	messageType, p, err := conn.ReadMessage()
	if err != nil {
		log.WithField("err", err).Println("error while reading websockets: ")
	}
	fmt.Println("messageType: ", messageType)
	fmt.Println("message: ", p)
}