package main

import (
	"github.com/spf13/viper"
)

func setConfig() (*viper.Viper, error){
	v := viper.New()
	v.SetConfigName("websocket")
	v.AddConfigPath("config")
	v.AddConfigPath(".")
	v.SetDefault("port", 5000)
	v.SetDefault("path", "/ws")
	v.SetDefault("host", "127.0.0.1")

	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}

	return v, nil
}